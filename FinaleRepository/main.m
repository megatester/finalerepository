//
//  main.m
//  FinaleRepository
//
//  Created by Алексей Ляшенкр on 14.11.17.
//  Copyright © 2017 Алексей Ляшенкo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
